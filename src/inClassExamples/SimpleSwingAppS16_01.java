package inClassExamples;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimpleSwingAppS16_01 {
	JTextField inputField;
	JTextArea outArea;

	public SimpleSwingAppS16_01() {
		
		JFrame frame = new JFrame("Our simple app");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container cont = frame.getContentPane();
		cont.setLayout(new BorderLayout());

		JButton button = new JButton("Push");

		inputField = new JTextField(20);
		
		Box buttonBox = Box.createHorizontalBox();
		cont.add(buttonBox, BorderLayout.SOUTH);
		buttonBox.add(button);
		buttonBox.add(inputField);

		outArea = new JTextArea(20, 60);
		outArea.setEditable(false);
		cont.add(outArea, BorderLayout.CENTER);

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String s = inputField.getText();
				outArea.append("\n" + s);
			}
		});

		frame.pack();
		frame.setSize(400, 300);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		new SimpleSwingAppS16_01();
	}

}
