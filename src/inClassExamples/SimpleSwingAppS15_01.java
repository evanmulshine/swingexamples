package inClassExamples;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class SimpleSwingAppS15_01 {	
	JFrame frame;
	private JTextArea jta;

	public static void main(String[] args) {
		new SimpleSwingAppS15_01();
	}
	
	public SimpleSwingAppS15_01() {
		frame = new JFrame("A simple application");
		frame.setLayout(new BorderLayout());
		
		Box buttonBox=Box.createHorizontalBox();
		frame.add(buttonBox, BorderLayout.SOUTH);
		
		
		JButton button = new JButton("Press me");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				jta.append("Button was clicked\n");
			}
		});
		buttonBox.add(button);
		
		jta = new JTextArea(6, 60);
		jta.setEditable(false);
		JScrollPane jsp = new JScrollPane(jta);
		frame.add(jsp,BorderLayout.CENTER);
		
		
		frame.pack();
		frame.setSize(new Dimension(800, 600));
		frame.setVisible(true);
		
	}
}
