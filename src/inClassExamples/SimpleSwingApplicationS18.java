package inClassExamples;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SimpleSwingApplicationS18 {
	
	private JButton otherButton;
	
	
	public static void main(String[] args) {
		new SimpleSwingApplicationS18();
	}
	public SimpleSwingApplicationS18() {
		JFrame frame = new JFrame("My Simple Application");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setLayout(new BorderLayout());
		Container cont = frame.getContentPane();

		
		JTextArea outputArea = new JTextArea(10, 10);
		outputArea.setEditable(false);
		outputArea.setLineWrap(true);
		cont.add(outputArea,BorderLayout.CENTER);
		
		
		JButton theButton = new JButton("Press me");

		JPanel buttonPanel = new JPanel();
		buttonPanel.add(theButton);
		cont.add(buttonPanel, BorderLayout.NORTH);
		
		JTextField inputField = new JTextField(12);
		buttonPanel.add(inputField);
				
		theButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				outputArea.append(inputField.getText()+"\n");
			}
		});
		
		otherButton = new JButton("Don't press me!");
		buttonPanel.add(otherButton);
		otherButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				outputArea.setText("");
			}
		});
		otherButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				outputArea.setText("Foo");
			}
		});
		
		frame.pack();
		frame.setSize(400,400);
		frame.setVisible(true);
	}

}
