package inClassExamples;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class SwingExampleS19 {
	
	private JTextArea jta;
	public static void main(String[] args) {
		SwingExampleS19 app = new SwingExampleS19();
	}
	public SwingExampleS19() {
		JFrame frame = new JFrame("My Simple App");
		frame.setLayout(new BorderLayout());

		jta = new JTextArea(40, 20);
		
		JButton button = new JButton("Press me");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jta.append("\nfoo");
				jta.append("\nblah");
			}
		});
		
		Box buttonBox = Box.createHorizontalBox();
		buttonBox.add(button);
		
		
		Container pane = frame.getContentPane();
		pane.add(buttonBox, BorderLayout.NORTH);
		pane.add(jta,BorderLayout.CENTER);
		jta.setText("Hello\nFoo");
		
		frame.pack();
		frame.setSize(600, 600);
		frame.setVisible(true);
	}

}
